import { AppRegistry } from 'react-native';
import App from './App';

AppRegistry.registerComponent('MetroExample', () => App);
