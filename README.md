# README #

Демонстрационный проект использования Metro, как замены RN Packager с поддержкой Typescript и Sourcemaps

### Установка? ###

* В корневой директории выполнить команду: 
```bash 
nvm use
npm i
```
* Выполнить команды: 
```bash 
gem install bundler
bundle
bundle exec pod install
```
* В корневой директории выполнить команду: 
```bash 
npm start --reset-cache
```